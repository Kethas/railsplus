package kethas.railsplus.tile;

import kethas.railsplus.BlastFurnaceRecipe;
import kethas.railsplus.RailsPlusConfig;
import kethas.railsplus.proxy.CommonProxy;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.*;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.NonNullList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.EnumSkyBlock;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

//big thanks to the vanilla furnace class for making this possible. I really feel like this is unnecessarily hard.
public class TileEntityBlastFurnace extends TileEntity implements ITickable {

    public static final int WIDTH = 3, HEIGHT = 3;

    public static final int[] FUEL_SLOTS = new int[]{0, 1};
    public static final int[] INPUT_SLOTS = new int[]{2};
    public static final int[] OUTPUT_SLOTS = new int[]{3};

    public static final int NUM_FUEL_SLOTS = 2;
    public static final int NUM_INPUT_SLOTS = 1;
    public static final int NUM_OUTPUT_SLOTS = 1;
    public static final int TOTAL_SLOTS = NUM_FUEL_SLOTS + NUM_INPUT_SLOTS + NUM_OUTPUT_SLOTS;

    public static final int[] BOTTOM_SLOTS = new int[]{0, 1, 3};
    public static final int[] SIDE_SLOTS = new int[]{0, 1, 2, 3};
    public static final int[] TOP_SLOTS = new int[]{2};

    public static final int MAX_TEMP = 1300;
    @CapabilityInject(IItemHandler.class)
    public static Capability<IItemHandler> ITEM_HANDLER_CAPABILITY;
    private ItemStackHandler inventory;
    private float timeLeft = 0;
    private int fuelPending = 0;
    private int temperature = 0;
    private boolean isController = false;
    private TileEntityBlastFurnace controller = null;
    private IItemHandler bottomInventory = new IItemHandler() {

        private boolean containsSlot(int index) {
            for (int i : BOTTOM_SLOTS)
                if (i == index) return true;

            return false;
        }

        @Override
        public int getSlots() {
            return getInventory().getSlots();
        }

        @SuppressWarnings("ConstantConditions")
        @Nonnull //<-- Yet the FAKE NEWS documentation says otherwise. Sad.
        @Override
        public ItemStack getStackInSlot(int slot) {
            return containsSlot(slot) ? getInventory().getStackInSlot(slot) : null;
        }

        @Nonnull
        @Override
        public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
            if (!containsSlot(slot))
                return stack;

            return getInventory().insertItem(slot, stack, simulate);
        }

        @Nonnull
        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate) {
            if (!containsSlot(slot))
                return ItemStack.EMPTY;

            return getInventory().extractItem(slot, amount, simulate);
        }

        @Override
        public int getSlotLimit(int slot) {
            if (!containsSlot(slot))
                return 0;

            return getInventory().getSlotLimit(slot);
        }
    };
    private IItemHandler sideInventory = new IItemHandler() {

        private boolean containsSlot(int index) {
            for (int i : SIDE_SLOTS)
                if (i == index) return true;

            return false;
        }

        @Override
        public int getSlots() {
            return getInventory().getSlots();
        }

        @SuppressWarnings("ConstantConditions")
        @Nonnull //<-- Yet the FAKE NEWS documentation says otherwise. Sad.
        @Override
        public ItemStack getStackInSlot(int slot) {
            return containsSlot(slot) ? getInventory().getStackInSlot(slot) : null;
        }

        @Nonnull
        @Override
        public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
            if (!containsSlot(slot))
                return stack;

            return getInventory().insertItem(slot, stack, simulate);
        }

        @Nonnull
        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate) {
            if (!containsSlot(slot))
                return ItemStack.EMPTY;

            return getInventory().extractItem(slot, amount, simulate);
        }

        @Override
        public int getSlotLimit(int slot) {
            if (!containsSlot(slot))
                return 0;

            return getInventory().getSlotLimit(slot);
        }
    };
    private IItemHandler topInventory = new IItemHandler() {

        private boolean containsSlot(int index) {
            for (int i : TOP_SLOTS)
                if (i == index) return true;

            return false;
        }

        @Override
        public int getSlots() {
            return getInventory().getSlots();
        }

        @SuppressWarnings("ConstantConditions")
        @Nonnull //<-- Yet the FAKE NEWS documentation says otherwise. Sad.
        @Override
        public ItemStack getStackInSlot(int slot) {
            return containsSlot(slot) ? getInventory().getStackInSlot(slot) : null;
        }

        @Nonnull
        @Override
        public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
            if (!containsSlot(slot))
                return stack;

            return getInventory().insertItem(slot, stack, simulate);
        }

        @Nonnull
        @Override
        public ItemStack extractItem(int slot, int amount, boolean simulate) {
            if (!containsSlot(slot))
                return ItemStack.EMPTY;

            return getInventory().extractItem(slot, amount, simulate);
        }

        @Override
        public int getSlotLimit(int slot) {
            if (!containsSlot(slot))
                return 0;

            return getInventory().getSlotLimit(slot);
        }
    };
    private BlockPos controllerPos;


    public static float tempSpeedFunc(int temperature) {
        return (float) (RailsPlusConfig.blastFurnaceSpeedModifier * (temperature < 900 ? 0 : (float) (((temperature - 900) / (MAX_TEMP - 900 * 0.9)) * 1.5 + 1)));
    }

    public static int getItemBurnTime(ItemStack stack) {
        if (stack.isEmpty()) {
            return 0;
        } else {
            int burnTime = net.minecraftforge.event.ForgeEventFactory.getItemBurnTime(stack);
            if (burnTime >= 0) return burnTime;
            Item item = stack.getItem();

            if (item == Item.getItemFromBlock(Blocks.WOODEN_SLAB)) {
                return 150;
            } else if (item == Item.getItemFromBlock(Blocks.WOOL)) {
                return 100;
            } else if (item == Item.getItemFromBlock(Blocks.CARPET)) {
                return 67;
            } else if (item == Item.getItemFromBlock(Blocks.LADDER)) {
                return 300;
            } else if (item == Item.getItemFromBlock(Blocks.WOODEN_BUTTON)) {
                return 100;
            } else if (Block.getBlockFromItem(item).getDefaultState().getMaterial() == Material.WOOD) {
                return 300;
            } else if (item == Item.getItemFromBlock(Blocks.COAL_BLOCK)) {
                return 16000;
            } else if (item instanceof ItemTool && "WOOD".equals(((ItemTool) item).getToolMaterialName())) {
                return 200;
            } else if (item instanceof ItemSword && "WOOD".equals(((ItemSword) item).getToolMaterialName())) {
                return 200;
            } else if (item instanceof ItemHoe && "WOOD".equals(((ItemHoe) item).getMaterialName())) {
                return 200;
            } else if (item == Items.STICK) {
                return 100;
            } else if (item != Items.BOW && item != Items.FISHING_ROD) {
                if (item == Items.SIGN) {
                    return 200;
                } else if (item == Items.COAL) {
                    return 1600;
                } else if (item == Items.LAVA_BUCKET) {
                    return 20000;
                } else if (item != Item.getItemFromBlock(Blocks.SAPLING) && item != Items.BOWL) {
                    if (item == Items.BLAZE_ROD) {
                        return 2400;
                    } else if (item instanceof ItemDoor && item != Items.IRON_DOOR) {
                        return 200;
                    } else {
                        return item instanceof ItemBoat ? 400 : 0;
                    }
                } else {
                    return 100;
                }
            } else {
                return 300;
            }
        }
    }

    public ItemStackHandler createInventory(NonNullList<ItemStack> inventory) {
        return new ItemStackHandler(inventory) {

            @Nonnull
            @Override
            public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
                if (slot == 3 && stack != ItemStack.EMPTY) { //you can't insert into the output slot!
                    return stack;
                }

                if ((slot == 0 || slot == 1) && getItemBurnTime(stack) == 0) {
                    return stack;
                }

                if (slot == 2 && CommonProxy.getBlastFurnaceRecipe(stack) == null) {
                    return stack;
                }

                return super.insertItem(slot, stack, simulate);
            }

            @Override
            protected void onContentsChanged(int slot) {
                super.onContentsChanged(slot);
                sendUpdates();
            }
        };
    }

    @Override
    public void update() {
        if (!isController)
            return;

        int oldTemp = temperature;

        if (fuelPending > 0) {
            fuelPending -= 2;
            temperature += 2;

            if (fuelPending < 0)
                fuelPending = 0;

            if (temperature > MAX_TEMP)
                temperature = MAX_TEMP;
        }

        if (temperature > 0)
            temperature--;

        if (temperature < 0)
            temperature = 0;


        if (temperature > 900 && hasValidRecipe()) { //can smelt
            BlastFurnaceRecipe recipe = CommonProxy.getBlastFurnaceRecipe(inventory.getStackInSlot(2));
            if (recipe != null) {
                if (timeLeft == 0 || timeLeft == -1) {
                    timeLeft = recipe.getTime();
                }

                timeLeft -= tempSpeedFunc(temperature);


                if (world.isRemote)
                    return;

                if (timeLeft <= 0) {
                    timeLeft = getTotalTime();
                    ItemStack stack = inventory.getStackInSlot(2);
                    stack.setCount(stack.getCount() - 1);
                    inventory.setStackInSlot(2, stack);

                    stack = inventory.getStackInSlot(3);
                    if (stack.isEmpty()) {
                        stack = recipe.getOutput().copy();
                    } else {
                        stack.setCount(stack.getCount() + recipe.getOutput().getCount());
                    }

                    inventory.setStackInSlot(3, stack);
                    sendUpdates();
                }
            }
        } else {
            timeLeft = getTotalTime();
        }

        if (oldTemp > 100 && temperature < 100 || oldTemp < 100 && temperature > 100) {
            sendUpdates();
        }

        if (world.isRemote)
            return;


        consumeFuel(0);
        consumeFuel(1);


    }

    public boolean hasValidRecipe() {
        if (!isFormed())
            return false;
        if (!isController)
            return controller.hasValidRecipe();

        BlastFurnaceRecipe recipe = CommonProxy.getBlastFurnaceRecipe(inventory.getStackInSlot(2));
        return recipe != null && (inventory.getStackInSlot(3).isEmpty() || (inventory.getStackInSlot(3).isItemEqual(recipe.getOutput()) && inventory.getStackInSlot(3).getCount() + recipe.getOutput().getCount() <= 64));
    }

    protected void consumeFuel(int slot) {
        if (fuelPending > 0 || !hasValidRecipe())
            return;

        ItemStack stack = inventory.getStackInSlot(slot);
        int burnTime = (int) (getItemBurnTime(stack) * RailsPlusConfig.blastFurnaceFuelModifier);

        if (stack.isEmpty())
            return;

        if (temperature <= MAX_TEMP * 4.5 / 6) { //can refuel with no regard to efficiency
            stack.setCount(stack.getCount() - 1);
            inventory.setStackInSlot(slot, stack);

            fuelPending = burnTime / 4;
            temperature -= Math.min(30, burnTime / 4);

            sendUpdates();
        } else { //can refuel up to 125% efficiency
            double efficiency = (burnTime / 4) / (MAX_TEMP - temperature);
            if (efficiency <= 1.1) {
                stack.setCount(stack.getCount() - 1);
                inventory.setStackInSlot(slot, stack);

                fuelPending = burnTime / 4;
                temperature -= Math.min(30, burnTime / 4);

                sendUpdates();
            }
        }
    }

    public void checkIfFormed(boolean notifyNeighbours) {
        if (world.isRemote || isFormed())
            return;

        boolean formed = true;
        int validNeighbours = 0;

        base:
        for (int i = -(WIDTH / 2); i <= WIDTH / 2; i++) {
            for (int j = -1; j <= HEIGHT - 2; j++) {
                for (int k = -(WIDTH / 2); k <= WIDTH / 2; k++) {
                    if (i == 0 && j == 0 && k == 0) //you don't need to check yourself
                        continue;

                    BlockPos neighbour = pos.add(i, j, k);

                    TileEntity te = world.getTileEntity(neighbour);

                    if (te instanceof TileEntityBlastFurnace && !((TileEntityBlastFurnace) te).isFormed()) {
                        validNeighbours++;
                    } else {
                        formed = false;
                        break base;
                    }


                }
            }
        }


        if (formed && validNeighbours == WIDTH * WIDTH * HEIGHT - 1) {
            isController = true;
            controller = this;
            inventory = createInventory(NonNullList.withSize(TOTAL_SLOTS, ItemStack.EMPTY));
            temperature = 0;
            fuelPending = 0;
            timeLeft = 0;

            sendUpdates();


            for (int i = -(WIDTH / 2); i <= WIDTH / 2; i++) {
                for (int j = -1; j <= HEIGHT - 2; j++) {
                    for (int k = -(WIDTH / 2); k <= WIDTH / 2; k++) {
                        if (i == 0 && j == 0 && k == 0) //don't set the controller for yourself
                            continue;

                        BlockPos neighbour = pos.add(i, j, k);
                        TileEntity te = world.getTileEntity(neighbour);

                        if (te instanceof TileEntityBlastFurnace) {
                            ((TileEntityBlastFurnace) te).controller = this;
                            ((TileEntityBlastFurnace) te).sendUpdates();
                        }

                    }
                }
            }

            return;
        }


        if (notifyNeighbours) {
            for (int i = -(WIDTH / 2); i <= WIDTH / 2; i++) {
                for (int j = -HEIGHT; j <= HEIGHT; j++) {
                    for (int k = -(WIDTH / 2); k <= WIDTH / 2; k++) {
                        if (i == 0 && j == 0 && k == 0) //don't notify yourself...
                            continue;

                        BlockPos neighbour = pos.add(i, j, k);
                        TileEntity te = world.getTileEntity(neighbour);

                        if (te instanceof TileEntityBlastFurnace) {
                            ((TileEntityBlastFurnace) te).checkIfFormed(false);
                        }
                    }
                }
            }

        }
    }

    public void deform() {
        if (world.isRemote) return;

        if (!isController && controller != null) {
            controller.deform();
            controller.isController = false;
            controller.controller = null;
        } else if (isController) {
            //reset inv, hopefully items will drop as specified in BlockBlastFurnace
            inventory = null;
            temperature = 0;
            fuelPending = 0;
            timeLeft = 0;
            isController = false;
            controller = null;

            sendUpdates();

            for (int i = -(WIDTH / 2); i <= WIDTH / 2; i++) {
                for (int j = -1; j <= HEIGHT - 2; j++) {
                    for (int k = -(WIDTH / 2); k <= WIDTH / 2; k++) {
                        if (i == 0 && j == 0 && k == 0) //don't deform yourself
                            continue;

                        BlockPos neighbour = pos.add(i, j, k);
                        TileEntity te = world.getTileEntity(neighbour);

                        if (te instanceof TileEntityBlastFurnace && !world.isAirBlock(neighbour)) {
                            ((TileEntityBlastFurnace) te).controller = null;
                            ((TileEntityBlastFurnace) te).temperature = 0;
                            ((TileEntityBlastFurnace) te).fuelPending = 0;
                            ((TileEntityBlastFurnace) te).timeLeft = 0;
                            ((TileEntityBlastFurnace) te).sendUpdates();
                        }

                    }
                }
            }
        }
    }

    public int getState() {
        if (controller == null)
            return 0;

        for (EnumFacing facing : EnumFacing.HORIZONTALS) {
            if (pos.equals(controller.getPos().offset(facing, WIDTH / 2))) {
                return getTemperature() > 100 ? 2 : 1;
            }
        }

        return 0;
    }

    public boolean isFormed() {
        return controller != null && controller.isController;
    }

    @Override
    public void onLoad() {
        if (!isFormed() && controllerPos != null)
            controller = (TileEntityBlastFurnace) world.getTileEntity(controllerPos);

        checkIfFormed(true);
    }

    public void sendUpdates() {
        world.notifyBlockUpdate(pos, world.getBlockState(pos), world.getBlockState(pos), 3);
        world.scheduleBlockUpdate(pos, this.getBlockType(), 0, 0);
        world.checkLightFor(EnumSkyBlock.BLOCK, pos);
        markDirty();

        if (isController) {
            world.markBlockRangeForRenderUpdate(pos.add(-(WIDTH / 2) - 1, -1 - 1, -(WIDTH / 2) - 1), pos.add(WIDTH / 2 + 1, HEIGHT - 2 + 1, WIDTH / 2 + 1));
            for (int i = -(WIDTH / 2); i <= WIDTH / 2; i++) {
                for (int j = -1; j <= HEIGHT - 2; j++) {
                    for (int k = -(WIDTH / 2); k <= WIDTH / 2; k++) {
                        if (i == 0 && j == 0 && k == 0) continue;

                        TileEntity te = world.getTileEntity(new BlockPos(i, j, k));

                        if (te instanceof TileEntityBlastFurnace) {
                            ((TileEntityBlastFurnace) te).sendUpdates();
                        }
                    }
                }
            }
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);


        if (compound.getBoolean("railsplus:blastfurnace_formed")) {
            isController = compound.getBoolean("railsplus:blastfurnace_iscontroller");
            if (isController) {
                controller = this;
            } else if (compound.hasKey("railsplus:blastfurnace_controller")) {
                controllerPos = BlockPos.fromLong(compound.getLong("railsplus:blastfurnace_controller"));
            }
        } else {
            isController = false;
            controller = null;
            controllerPos = null;
        }

        if (compound.hasKey("railsplus:blastfurnace_inv")) {
            if (inventory == null)
                inventory = createInventory(NonNullList.withSize(TOTAL_SLOTS, ItemStack.EMPTY));

            inventory.deserializeNBT(compound.getCompoundTag("railsplus:blastfurnace_inv"));
        }

        timeLeft = compound.getFloat("railsplus:blastfurnace_timeleft");
        fuelPending = compound.getInteger("railsplus:blastfurnace_fuelpending");
        temperature = compound.getInteger("railsplus:blastfurnace_temperature");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);

        compound.setBoolean("railsplus:blastfurnace_formed", isFormed());

        if (isFormed()) {
            compound.setBoolean("railsplus:blastfurnace_iscontroller", isController);

            if (controller != null)
                compound.setLong("railsplus:blastfurnace_controller", controller.pos.toLong());
        }


        if (inventory != null) {
            compound.setTag("railsplus:blastfurnace_inv", inventory.serializeNBT());
        }


        compound.setFloat("railsplus:blastfurnace_timeleft", timeLeft);
        compound.setInteger("railsplus:blastfurnace_fuelpending", fuelPending);
        compound.setInteger("railsplus:blastfurnace_temperature", temperature);

        return compound;
    }

    @Nullable
    @Override
    public SPacketUpdateTileEntity getUpdatePacket() {
        return new SPacketUpdateTileEntity(pos, 3, getUpdateTag());
    }

    @Override
    public NBTTagCompound getUpdateTag() {
        return writeToNBT(new NBTTagCompound());
    }

    @Override
    public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
        handleUpdateTag(pkt.getNbtCompound());

        markDirty();
        IBlockState state = getWorld().getBlockState(getPos());
        getWorld().notifyBlockUpdate(getPos(), state, state, 3);
    }

    @Override
    public void handleUpdateTag(NBTTagCompound tag) {
        super.handleUpdateTag(tag);

        if (!isFormed() && controllerPos != null)
            controller = (TileEntityBlastFurnace) world.getTileEntity(controllerPos);


        if (isController) {
            for (int i = -(WIDTH / 2); i <= WIDTH / 2; i++) {
                for (int j = -1; j <= HEIGHT - 2; j++) {
                    for (int k = -(WIDTH / 2); k <= WIDTH / 2; k++) {
                        if (i == 0 && j == 0 && k == 0) //don't set the controller for yourself
                            continue;

                        BlockPos neighbour = pos.add(i, j, k);
                        if (!world.isAirBlock(neighbour) && world.getBlockState(neighbour).getBlock() == CommonProxy.blockBlastFurnace) {
                            TileEntity te = world.getTileEntity(neighbour);

                            if (te instanceof TileEntityBlastFurnace) {
                                ((TileEntityBlastFurnace) te).controller = this;
                                ((TileEntityBlastFurnace) te).sendUpdates();
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        if (capability == ITEM_HANDLER_CAPABILITY) {
            return isController || controller != null;
        }

        return super.hasCapability(capability, facing);
    }

    @SuppressWarnings("unchecked")
    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        if (capability == ITEM_HANDLER_CAPABILITY) {

            if (controller != null && !isController) {
                return controller.getCapability(capability, facing);
            } else if (isController) {

                if (facing == null) {
                    return (T) inventory;
                } else if (facing == EnumFacing.DOWN) {
                    return (T) bottomInventory;
                } else if (facing == EnumFacing.UP) {
                    return (T) topInventory;
                } else {
                    return (T) sideInventory;
                }
            }
        }

        return super.getCapability(capability, facing);
    }

    public ItemStackHandler getInventory() {

        if (!isController) {
            return controller == null ? null : controller.getInventory();
        }
        return inventory;
    }

    public boolean isController() {
        return isController;
    }

    public TileEntityBlastFurnace getController() {
        return controller;
    }

    public int getTemperature() {
        if (controller == null)
            return -1;

        if (!isController)
            return controller.getTemperature();

        return temperature;
    }

    public float getTimeLeft() {
        if (controller == null)
            return -1;

        if (!isController)
            return controller.getTimeLeft();

        return timeLeft;
    }

    public int getTotalTime() {
        if (controller == null)
            return -1;

        if (!isController)
            return controller.getTotalTime();

        BlastFurnaceRecipe recipe = CommonProxy.getBlastFurnaceRecipe(inventory.getStackInSlot(2));

        if (recipe == null)
            return -1;

        return recipe.getTime();
    }
}

