package kethas.railsplus.tile;

import kethas.railsplus.RailsPlus;
import kethas.railsplus.proxy.CommonProxy;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.util.ResourceLocation;

import static net.minecraft.client.renderer.GlStateManager.color;

public class GuiBlastFurnace extends GuiContainer {

    public static final ResourceLocation BACKGROUND = new ResourceLocation(RailsPlus.MODID, "textures/gui/blast_furnace.png");
    private final InventoryPlayer playerInventory;

    private ContainerBlastFurnace containerBlastFurnace;

    public GuiBlastFurnace(ContainerBlastFurnace inventorySlotsIn, InventoryPlayer playerInventory) {
        super(inventorySlotsIn);
        containerBlastFurnace = inventorySlotsIn;
        this.playerInventory = playerInventory;
    }

    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        color(1, 1, 1, 1);
        mc.getTextureManager().bindTexture(BACKGROUND);
        int x = (width - xSize) / 2;
        int y = (height - ySize) / 2;
        drawTexturedModalRect(x, y, 0, 0, xSize, ySize);

        int fire = fireFunc(containerBlastFurnace.blastFurnace.getTemperature());
        drawTexturedModalRect(x + 56, y + 40 + 12 - fire, 176, 13 - fire, 14, fire);


        if (containerBlastFurnace.blastFurnace.getController().hasValidRecipe() && containerBlastFurnace.blastFurnace.getTimeLeft() != -1) {

            int arrow = containerBlastFurnace.blastFurnace.getTotalTime();

            arrow = Math.round(Math.max(Math.min(1, ((arrow - containerBlastFurnace.blastFurnace.getTimeLeft()) / arrow)), 0) * 24);

            drawTexturedModalRect(x + 75, y + 37, 176, 14, arrow + 1, 16);


        }

    }

    private int fireFunc(int temperature) {
        if (temperature < 900 && temperature > 0) {
            return 1;
        } else if (temperature == 0) {
            return 0;
        } else {
            return (int) Math.round(Math.min(1.0, (temperature - 900.0) / (TileEntityBlastFurnace.MAX_TEMP - 1000.0)) * 12 + 1);
        }
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        String name = I18n.format(CommonProxy.blockBlastFurnace.getUnlocalizedName() + ".name");
        fontRenderer.drawString(name, xSize / 2 - fontRenderer.getStringWidth(name) / 2, 6, 0x404040);
        fontRenderer.drawString(playerInventory.getDisplayName().getUnformattedText(), 8, ySize - 94, 0x404040);

        if (isPointInRegion(56, 40, 14, 13, mouseX, mouseY)) {
            drawHoveringText(Math.round(containerBlastFurnace.blastFurnace.getTemperature() / 10) * 10 + "C", 56, 40);
        }
        /*if (isPointInRegion(75, 37, 25, 16, mouseX, mouseY)) {
            float percent = ((containerBlastFurnace.blastFurnace.getTotalTime() - containerBlastFurnace.blastFurnace.getTimeLeft()) / containerBlastFurnace.blastFurnace.getTotalTime());

            drawHoveringText(Math.round(percent * 100) + "%", 75, 37);
        }*/

    }


}
