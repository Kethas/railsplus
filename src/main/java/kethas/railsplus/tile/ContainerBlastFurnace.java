package kethas.railsplus.tile;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerBlastFurnace extends Container {

    TileEntityBlastFurnace blastFurnace;
    private InventoryPlayer playerInventory;

    public ContainerBlastFurnace(InventoryPlayer playerInventory, TileEntityBlastFurnace blastFurnace) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 9; j++) {
                addSlotToContainer(new Slot(playerInventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (int k = 0; k < 9; k++) {
            addSlotToContainer(new Slot(playerInventory, k, 8 + k * 18, 142));
        }


        this.blastFurnace = blastFurnace;
        this.playerInventory = playerInventory;

        if (blastFurnace.getInventory() == null) {
            System.out.println("inv is null");
            return;
        }

        //fuel slot 1
        addSlotToContainer(new SlotItemHandler(blastFurnace.getInventory(), 0, 46, 56) {
            @Override
            public void onSlotChanged() {
                blastFurnace.sendUpdates();
            }
        });

        //fuel slot 2
        addSlotToContainer(new SlotItemHandler(blastFurnace.getInventory(), 1, 64, 56) {
            @Override
            public void onSlotChanged() {
                blastFurnace.sendUpdates();
            }
        });

        //input slot 1
        addSlotToContainer(new SlotItemHandler(blastFurnace.getInventory(), 2, 55, 18) {
            @Override
            public void onSlotChanged() {
                blastFurnace.sendUpdates();
            }
        });

        //output slot 1
        addSlotToContainer(new SlotItemHandler(blastFurnace.getInventory(), 3, 112, 37) {
            @Override
            public void onSlotChanged() {
                blastFurnace.sendUpdates();
            }
        });
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return blastFurnace.isFormed();
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer playerIn, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot slot = inventorySlots.get(index);

        if (slot != null && slot.getHasStack()) {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            int containerSlots = inventorySlots.size() - playerIn.inventory.mainInventory.size();

            if (index < containerSlots) {
                if (!this.mergeItemStack(itemstack1, containerSlots, inventorySlots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.mergeItemStack(itemstack1, 0, containerSlots, false)) {
                return ItemStack.EMPTY;
            }

            if (itemstack1.getCount() == 0) {
                slot.putStack(ItemStack.EMPTY);
            } else {
                slot.onSlotChanged();
            }

            if (itemstack1.getCount() == itemstack.getCount()) {
                return ItemStack.EMPTY;
            }

            slot.onTake(playerIn, itemstack1);
        }

        return itemstack;
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        if (blastFurnace.hasValidRecipe() || blastFurnace.getTemperature() > 0)
            blastFurnace.sendUpdates();
    }
}
