package kethas.railsplus;

import net.minecraftforge.common.config.Config;

import java.util.HashMap;
import java.util.Map;

@Config(modid = RailsPlus.MODID)
public class RailsPlusConfig {

    @Config.Comment({
            "Values between 0 - 1 make fuel less efficient in the blast furnace.",
            "Values above 1 make fuel more efficient in the blast furnace.",
            "Default is 1 (no change)."
    })
    public static double blastFurnaceFuelModifier = 1;

    @Config.Comment({
            "Values between 0 - 1 make blast furnace recipes slower.",
            "Values above 1 make blast furnace recipes faster.",
            "Default is 1 (no change)."
    })
    public static double blastFurnaceSpeedModifier = 1;

    @Config.Comment("Set to false to disable the use of vanilla furnace recipes in the blast furnace.")
    public static boolean enableFurnaceRecipesInBlastFurnace = true;

    @Config.Comment({
            "Add or delete entries to create ore doubling recipes like so.",
            "Names starting with '@' will use the OreDictionary instead.",
            "For every OreDictionary entry for the input, a recipe will be created.",
            "The output (if an OreDictionary entry) uses the first entry.",
            "If in either the input or output there are no items for the OreDictionary entry, the recipe will not be created."
    })
    public static Map<String, String> oreDoublingRecipes = new HashMap<String, String>() {
        {
            put("minecraft:iron_ore", "minecraft:iron_ingot");
            put("minecraft:gold_ore", "minecraft:gold_ingot");
            put("@oreCopper", "@ingotCopper");
            put("@oreTin", "@ingotTin");
            put("@oreSilver", "@ingotSilver");
            put("@oreLead", "@ingotLead");
            put("@oreAluminum", "@ingotAluminum");
            put("@oreNickel", "@ingotNickel");
            put("@orePlatinum", "@ingotPlatinum");
            put("@oreIridium", "@ingotIridium");
        }
    };

}
