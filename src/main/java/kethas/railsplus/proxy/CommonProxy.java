package kethas.railsplus.proxy;

import kethas.railsplus.BlastFurnaceRecipe;
import kethas.railsplus.RailsPlus;
import kethas.railsplus.RailsPlusConfig;
import kethas.railsplus.RailsPlusGuiHandler;
import kethas.railsplus.block.BlockBlastFurnace;
import kethas.railsplus.item.ItemCoke;
import kethas.railsplus.item.ItemSteelIngot;
import kethas.railsplus.rail.RailSteel;
import kethas.railsplus.rail.RailSteelBrake;
import kethas.railsplus.rail.RailSteelPowered;
import kethas.railsplus.tile.TileEntityBlastFurnace;
import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static net.minecraftforge.fml.common.eventhandler.EventPriority.HIGHEST;

/**
 * Created by Kethas on 11/02/2017.
 */
public class CommonProxy {

    public static IForgeRegistry<BlastFurnaceRecipe> BLAST_FURNACE_RECIPE_REGISTRY;

    //creative tabs
    public static CreativeTabs railsPlus;
    public static CreativeTabs railsPlusTransportation;

    //items
    public static ItemSteelIngot itemSteelIngot;
    public static ItemCoke itemCoke;

    //blocks
    public static BlockBlastFurnace blockBlastFurnace;

    //rails
    public static RailSteel railSteel;
    public static RailSteelPowered railSteelPowered;
    public static RailSteelBrake railSteelBrake;

    public static BlastFurnaceRecipe getBlastFurnaceRecipe(ResourceLocation name) {
        return BLAST_FURNACE_RECIPE_REGISTRY.getValue(name);
    }

    public static BlastFurnaceRecipe getBlastFurnaceRecipe(ItemStack item) {
        return GameRegistry.findRegistry(BlastFurnaceRecipe.class).getValue(new ResourceLocation(item.getItem().getRegistryName().getResourceDomain(), item.getItem().getRegistryName().getResourcePath() + "_" + item.getItemDamage()));

        /*
        if (GameRegistry.findRegistry(BlastFurnaceRecipe.class) == null) {
            System.err.println("null registry");
            return null;
        }

        for (BlastFurnaceRecipe recipe : BLAST_FURNACE_RECIPE_REGISTRY.getValues()) {
            if (recipe.getInput().isItemEqual(item))
                return recipe;
        }

        return null;
        */
    }

    public void init(FMLInitializationEvent event) {
        registerRecipes();
    }

    public void postInit(FMLPostInitializationEvent event) {

    }

    public void preInit(FMLPreInitializationEvent event) {
        railsPlus = new CreativeTabs("railsplus.general") {
            @Override
            public ItemStack getTabIconItem() {
                return new ItemStack(itemSteelIngot);
            }
        };

        railsPlusTransportation = new CreativeTabs("railsplus.transportation") {
            @Override
            public ItemStack getTabIconItem() {
                return new ItemStack(ItemBlock.getItemFromBlock(railSteelPowered));
            }
        };

        //but why do i have to call it??? it doesnt work any other way!
        onNewRegistry(new RegistryEvent.NewRegistry());

        MinecraftForge.EVENT_BUS.register(this);


        itemSteelIngot = new ItemSteelIngot();
        itemCoke = new ItemCoke();

        blockBlastFurnace = new BlockBlastFurnace();

        railSteel = new RailSteel();
        railSteelPowered = new RailSteelPowered();
        railSteelBrake = new RailSteelBrake();

        //fuelHandler = new FuelHandler();
        //GameRegistry.registerFuelHandler(fuelHandler);

        NetworkRegistry.INSTANCE.registerGuiHandler(RailsPlus.instance, new RailsPlusGuiHandler());
    }

    @SubscribeEvent
    protected void onRegisterItems(RegistryEvent.Register<Item> e) {
        IForgeRegistry<Item> registry = e.getRegistry();

        registry.register(itemSteelIngot);
        registry.register(itemCoke);

        registry.register(blockBlastFurnace.getItemBlock());

        registry.register(railSteel.getItemBlock());
        registry.register(railSteelPowered.getItemBlock());
        registry.register(railSteelBrake.getItemBlock());
    }

    @SuppressWarnings("deprecation")
    @SubscribeEvent
    protected void onRegisterBlocks(RegistryEvent.Register<Block> e) {
        IForgeRegistry<Block> registry = e.getRegistry();

        registry.register(blockBlastFurnace);

        registry.register(railSteel);
        registry.register(railSteelPowered);
        registry.register(railSteelBrake);

        GameRegistry.registerTileEntity(TileEntityBlastFurnace.class, blockBlastFurnace.getRegistryName().toString());
    }

    @SubscribeEvent(priority = HIGHEST)
    protected void onRegisterBlastFurnaceRecipes(RegistryEvent.Register<BlastFurnaceRecipe> e) {
        IForgeRegistry<BlastFurnaceRecipe> registry = e.getRegistry();

        registry.register(new BlastFurnaceRecipe() { //coke
            @Nonnull
            @Override
            public ItemStack getInput() {
                return new ItemStack(Items.COAL);
            }

            @Nonnull
            @Override
            public ItemStack getOutput() {
                return new ItemStack(itemCoke);
            }

            @Override
            public int getTime() {
                return 200;
            }
        });

        registry.register(new BlastFurnaceRecipe() { //steel
            @Nonnull
            @Override
            public ItemStack getInput() {
                return new ItemStack(Items.IRON_INGOT);
            }

            @Nonnull
            @Override
            public ItemStack getOutput() {
                return new ItemStack(itemSteelIngot);
            }

            @Override
            public int getTime() {
                return 200;
            }
        });


        if (RailsPlusConfig.enableFurnaceRecipesInBlastFurnace) {
            for (Map.Entry<ItemStack, ItemStack> entry : FurnaceRecipes.instance().getSmeltingList().entrySet()) {
                registry.register(new BlastFurnaceRecipe() {
                    @Nonnull
                    @Override
                    public ItemStack getInput() {
                        return entry.getKey();
                    }

                    @Nonnull
                    @Override
                    public ItemStack getOutput() {
                        return entry.getValue();
                    }

                    @Override
                    public int getTime() {
                        return 200;
                    }
                });
            }
        }


        for (Map.Entry<String, String> entry : RailsPlusConfig.oreDoublingRecipes.entrySet()) {

            List<ItemStack> inputs, outputs;

            if (entry.getKey().startsWith("@"))
                inputs = OreDictionary.getOres(entry.getKey().substring(1), false);
            else
                inputs = Collections.singletonList(new ItemStack(GameRegistry.findRegistry(Item.class).getValue(new ResourceLocation(entry.getKey()))));


            if (entry.getKey().startsWith("@")) {
                outputs = OreDictionary.getOres(entry.getValue().substring(1), false);

                if (!outputs.isEmpty()) {
                    outputs = outputs.subList(0, 1);
                }
            } else {
                outputs = Collections.singletonList(new ItemStack(GameRegistry.findRegistry(Item.class).getValue(new ResourceLocation(entry.getValue()))));
            }


            for (ItemStack input : inputs) {
                if (input == null) continue;
                input.setCount(1);
                for (ItemStack output : outputs) {
                    if (output == null) continue;
                    output.setCount(2);

                    registry.register(new BlastFurnaceRecipe() {
                        @Nonnull
                        @Override
                        public ItemStack getInput() {
                            return input.copy();
                        }

                        @Nonnull
                        @Override
                        public ItemStack getOutput() {
                            return output.copy();
                        }

                        @Override
                        public int getTime() {
                            return 200;
                        }
                    });
                }
            }
        }
    }

    @SubscribeEvent
    protected void onNewRegistry(RegistryEvent.NewRegistry e) {
        System.out.println("creating blast furnace registry");
        BLAST_FURNACE_RECIPE_REGISTRY = new RegistryBuilder<BlastFurnaceRecipe>()
                .setType(BlastFurnaceRecipe.class)
                .setName(new ResourceLocation(RailsPlus.MODID, "blastfurnace_recipes"))
                //.setDefaultKey(new ResourceLocation(RailsPlus.MODID, "blastfurnace_recipes"))
                .allowModification()
                .create();

    }

    protected void registerRecipes() {
        OreDictionary.registerOre("ingotSteel", itemSteelIngot);
        OreDictionary.registerOre("coke", itemCoke);

        //GameRegistry.addSmelting(Items.COAL, new ItemStack(itemCoke, 1), 0);
        //GameRegistry.addSmelting(Items.IRON_INGOT, new ItemStack(itemSteelIngot, 1), 0);

    }
}
