package kethas.railsplus;

import kethas.railsplus.tile.ContainerBlastFurnace;
import kethas.railsplus.tile.GuiBlastFurnace;
import kethas.railsplus.tile.TileEntityBlastFurnace;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

import javax.annotation.Nullable;

public class RailsPlusGuiHandler implements IGuiHandler {

    public static final int BLAST_FURNACE = 0;

    @Nullable
    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case BLAST_FURNACE:
                return new ContainerBlastFurnace(player.inventory, (TileEntityBlastFurnace) world.getTileEntity(new BlockPos(x, y, z)));
        }

        return null;
    }

    @Nullable
    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case BLAST_FURNACE:
                return new GuiBlastFurnace((ContainerBlastFurnace) getServerGuiElement(ID, player, world, x, y, z), player.inventory);
        }

        return null;
    }
}
