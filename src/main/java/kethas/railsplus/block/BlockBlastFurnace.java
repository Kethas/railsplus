package kethas.railsplus.block;

import kethas.railsplus.RailsPlus;
import kethas.railsplus.RailsPlusGuiHandler;
import kethas.railsplus.proxy.CommonProxy;
import kethas.railsplus.tile.TileEntityBlastFurnace;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.IProperty;
import net.minecraft.block.properties.PropertyInteger;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import org.lwjgl.input.Keyboard;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Random;

/**
 * Created by Kethas on 11/02/2017.
 */
public class BlockBlastFurnace extends BlockBase implements ITileEntityProvider {

    public static final IProperty<Integer> FURNACE_STATE = PropertyInteger.create("furnace_state", 0, 2);

    public BlockBlastFurnace() {
        super("blast_furnace", Material.IRON);
        setCreativeTab(CommonProxy.railsPlus);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }

    @SuppressWarnings("deprecation")
    @Override
    public IBlockState getStateFromMeta(int meta) {
        return getDefaultState();
    }

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, FURNACE_STATE);
    }

    @SuppressWarnings("deprecation")
    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        TileEntity te = worldIn.getTileEntity(pos);

        if (te instanceof TileEntityBlastFurnace)
            return state.withProperty(FURNACE_STATE, ((TileEntityBlastFurnace) te).getState());

        return state;
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileEntityBlastFurnace();
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        TileEntityBlastFurnace te = (TileEntityBlastFurnace) worldIn.getTileEntity(pos);

        if (worldIn.isRemote) return te.isFormed();

        if (te.isFormed()) {

            if (!playerIn.isSneaking()) {
                playerIn.openGui(RailsPlus.instance, RailsPlusGuiHandler.BLAST_FURNACE, worldIn, pos.getX(), pos.getY(), pos.getZ());
            }

            return true;
        }

        return false;
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return Item.getItemFromBlock(this);
    }

    @Override
    public void breakBlock(World worldIn, BlockPos pos, IBlockState state) {
        TileEntity te = worldIn.getTileEntity(pos);

        if (te instanceof TileEntityBlastFurnace) {

            if (((TileEntityBlastFurnace) te).isFormed()) {

                for (int i = 0; i < ((TileEntityBlastFurnace) te).getInventory().getSlots(); i++) {
                    InventoryHelper.spawnItemStack(worldIn, pos.getX(), pos.getY(), pos.getZ(), ((TileEntityBlastFurnace) te).getInventory().getStackInSlot(i));
                }

                ((TileEntityBlastFurnace) te).deform();
            }
        }

        super.breakBlock(worldIn, pos, state);
    }

    @Override
    public void addInformation(ItemStack stack, @Nullable World player, List<String> tooltip, ITooltipFlag advanced) {
        super.addInformation(stack, player, tooltip, advanced);

        tooltip.add("\u00A79Multiblock Size: " + TileEntityBlastFurnace.WIDTH + "x" + TileEntityBlastFurnace.HEIGHT + "x" + TileEntityBlastFurnace.WIDTH);
        if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT)) {
            tooltip.add("\u00A79Total Blocks Required: " + (TileEntityBlastFurnace.WIDTH * TileEntityBlastFurnace.WIDTH * TileEntityBlastFurnace.HEIGHT));
        }
    }
}
