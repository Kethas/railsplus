package kethas.railsplus;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.FMLContainer;
import net.minecraftforge.fml.common.InjectedModContainer;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import net.minecraftforge.registries.IForgeRegistryEntry;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public abstract class BlastFurnaceRecipe implements IForgeRegistryEntry<BlastFurnaceRecipe> {

    private ResourceLocation registryName;

    public BlastFurnaceRecipe() {
        if (!getInput().isEmpty()) {
            setRegistryName(getInput().getItem().getRegistryName() + "_" + getInput().getItemDamage());
        }
    }

    @Nonnull
    public abstract ItemStack getInput();

    @Nonnull
    public abstract ItemStack getOutput();

    public abstract int getTime();

    public BlastFurnaceRecipe setRegistryName(String name) {
        if (getRegistryName() != null)
            throw new IllegalStateException("Attempted to set registry name with existing registry name! New: " + name + " Old: " + getRegistryName());

        int index = name.lastIndexOf(':');
        String oldPrefix = index == -1 ? "" : name.substring(0, index);
        name = index == -1 ? name : name.substring(index + 1);
        ModContainer mc = Loader.instance().activeModContainer();
        String prefix = mc == null || (mc instanceof InjectedModContainer && ((InjectedModContainer) mc).wrappedContainer instanceof FMLContainer) ? "minecraft" : mc.getModId().toLowerCase();
        if (!oldPrefix.equals(prefix) && oldPrefix.length() > 0) {
            prefix = oldPrefix;
        }

        registryName = new ResourceLocation(prefix, name);
        return this;
    }

    public final BlastFurnaceRecipe setRegistryName(String modID, String name) {
        return setRegistryName(modID + ":" + name);
    }

    @Override

    @Nullable
    public final ResourceLocation getRegistryName() {
        return registryName;
    }

    //Helper functions
    @Override
    public final BlastFurnaceRecipe setRegistryName(ResourceLocation name) {
        return setRegistryName(name.toString());
    }

    @Override
    public Class<BlastFurnaceRecipe> getRegistryType() {
        return BlastFurnaceRecipe.class;
    }
}
