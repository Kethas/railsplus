package kethas.railsplus.item;

import kethas.railsplus.proxy.CommonProxy;
import net.minecraft.item.ItemStack;

/**
 * Created by Kethas on 12/02/2017.
 */
public class ItemCoke extends ItemBase {

    public ItemCoke() {
        super("coke");
        setCreativeTab(CommonProxy.railsPlus);
    }

    @Override
    public int getItemBurnTime(ItemStack itemStack) {
        return 3200;
    }
}
